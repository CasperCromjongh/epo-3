library IEEE;
use IEEE.std_logic_1164.ALL;
use IEEE.numeric_std.all;

architecture behaviour of goty_tb is

component gotygolddeluxe is
	port (
		xi		: in		std_logic;
		xo		: inout	std_logic;
		reset	: in		std_logic;
		left		: in		std_logic;
		right	: in		std_logic;
		red,green,blue: out    std_logic;
		hsync         : out    std_logic;
		vsync         : out    std_logic;
		rip           : inout  std_logic;
		cnt           : inout  std_logic;
		enable_reset  : in     std_logic
	);
end component;

signal clk, xo, reset, left, right, rip, cnt, enable_reset: std_logic;
signal red, green, blue : std_logic;
signal hsync, vsync : std_logic;

begin
	GOTY: gotygolddeluxe port map (
		xi => clk,
		xo => xo,
		reset => reset,
		left => left,
		right => right,
		red => red,
		green => green,
		blue => blue,
		hsync => hsync,
		vsync => vsync,
		rip => rip,
		cnt => cnt,
		enable_reset	 => enable_reset
	);
	
	clk <= '1' after 0 ns, '0' after 81.78 ns when clk /= '0' else '1' after 81.78 ns;
	reset <= '1', '0' after 100 ns, '1' after 2200 ms, '0' after 2210 ms;
	enable_reset <= '0', '1' after 16.8 ms, '0' after 34 ms;
	left <= '0', '1' after 10 ms; --, '0' after 560 ms, '1' after 1900 ms, '0' after 3000 ms;
	right <= '0';--, '1' after 700 ms, '0' after 1330 ms, '1' after 1500 ms, '0' after 1550 ms;
	
end behaviour;
