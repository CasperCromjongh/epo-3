library IEEE;
use IEEE.std_logic_1164.ALL;

entity hit_math_unit is
	port(clk			:in    std_logic;
		reset			:in    std_logic;
		start			:in    std_logic;
		x_circle		:in    std_logic_vector(6 downto 0);
		y_circle		:in    std_logic_vector(7 downto 0);
		x_block_0	:in    std_logic_vector(6 downto 0);
		x_block_1	:in    std_logic_vector(6 downto 0);
		y_block		:in    std_logic_vector(7 downto 0);
		height		:in    std_logic_vector(5 downto 0);
		value1		:out  std_logic_vector(7 downto 0);
		value2		:out  std_logic_vector(7 downto 0);
		result			:in    std_logic_vector(7 downto 0);
		started		:out  std_logic;
		done			:out  std_logic;
		rip				:out  std_logic;
		rip_reset	:in	std_logic
);
end hit_math_unit;




















