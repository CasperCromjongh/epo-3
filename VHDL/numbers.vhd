library IEEE;
use IEEE.std_logic_1164.ALL;
use IEEE.numeric_std.ALL;

entity numbers is
	port ( clk	: in std_logic;
	       reset : in std_logic;
	       score : out std_logic_vector(3 downto 0));
end numbers;