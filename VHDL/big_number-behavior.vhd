library IEEE;
use IEEE.std_logic_1164.ALL;
use IEEE.numeric_std.ALL;

architecture behaviour of big_number is
signal num_count, new_num_count : unsigned (3 downto 0);
signal score_count : std_logic_vector (3 downto 0);

begin
process (clk)
	begin
		if (clk'event and clk = '1') then
			if (reset = '1') then
				num_count <= (others => '0');
				score_count <= (others => '0');
			else 
				num_count <= new_num_count;
				score_count <= a;
			end if;
		end if;
	end process;

process (score_count, num_count) 
	begin

		if(score_count = "1010") then 
			new_num_count <= num_count + 1;
		elsif(num_count = "1010") then
			new_num_count <= "0000"; 
		else
			new_num_count <= num_count;
			
		end if;	
	end process;

b <= std_logic_vector(num_count);

end behaviour;
