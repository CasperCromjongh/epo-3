#define left_button 3
#define right_button 2
#define left_button_led 12
#define right_button_led 11
#define joy_up_pin 6
#define joy_down_pin 7
#define joy_left_pin 4
#define joy_right_pin 5

#define blink_pin 9

#define lights true

char printing[20];
uint8_t left_btn = 0, right_btn = 0, joy_up = 0, joy_left = 0, joy_down = 0, joy_right = 0;
uint32_t serial_write = 0, blink_time = 0;
bool lights_on =  false;

void setup() {
  Serial.begin(9600);
  Serial.print("Hello you");
  pinMode(left_button, INPUT);
  pinMode(right_button, INPUT);
  pinMode(joy_up_pin, INPUT);
  pinMode(joy_left_pin, INPUT);
  pinMode(joy_down_pin, INPUT);
  pinMode(joy_right_pin, INPUT);
  #if lights
    pinMode(left_button_led, OUTPUT);
    pinMode(right_button_led, OUTPUT);
  #endif
  
  pinMode(blink_pin, INPUT);
}

void loop() {
  left_btn = digitalRead(left_button);
  right_btn = digitalRead(right_button);
  joy_up = digitalRead(joy_up_pin);
  joy_left = digitalRead(joy_left_pin);
  joy_down = digitalRead(joy_down_pin);
  joy_right = digitalRead(joy_right_pin);
  if (millis() - serial_write >= 16) {
    sprintf(printing, "%d%d%d%d%d%d %d\n", left_btn, right_btn, joy_up, joy_left, joy_down, joy_right, lights_on);
    Serial.print(printing);
    serial_write = millis();
  }

  if (digitalRead(blink_pin)) {
    if (millis() - blink_time >= 700) {
      lights_on = !lights_on;
      blink_time = millis();
    }
  } else {
    lights_on =  false;
  }

  if (!left_btn != !lights_on) {
    digitalWrite(left_button_led, HIGH);
  } else {
    digitalWrite(left_button_led, LOW);
  }
  
  if (!right_btn != !lights_on) {
    digitalWrite(right_button_led, HIGH);
  } else {
    digitalWrite(right_button_led, LOW);
  }
}
